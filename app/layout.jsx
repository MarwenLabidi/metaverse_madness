import "./globals.css";
import localFont from "@next/font/local";

const EudoxusSansMedium = localFont({
        src: "../public/assets/fonts/EudoxusSans-Bold.woff2",
});

export default function RootLayout({ children }) {
        return (
                <html lang='en'>
                        <head />
                        <body className={EudoxusSansMedium.className}>
                                {children}
                        </body>
                </html>
        );
}
