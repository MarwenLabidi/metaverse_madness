import { main } from "./page.module.css";
import HEADER from "./Components/CL-Header";
import MAIN from "./Components/Main/index";
import FOOTER from "./Components/CL-Footer/index";

export default function Home() {
        return (
                <main className={main}>
                        <HEADER />
                        <MAIN />
                        <FOOTER />
                </main>
        );
}
