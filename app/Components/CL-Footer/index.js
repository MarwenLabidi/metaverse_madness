"use client";
import Image from "next/image";

import React from "react";
import {footer} from"./index.module.css"
const FOOTER = () => {
        return( 
        <footer className={footer}>
          <section>
            <h1>Enter the Metaverse</h1>
            <button>
            <Image
                                        src='/assets/headset.svg'
                                        alt='headset logo'
                                        width={25}
                                        height={25}
                                />
              <h3>Enter Metaverse</h3>
            </button>
          </section>
          <hr/>
          <section>
          <h2>METAVERSUS</h2>
          <p>Copyright © 2021 - 2022 Metaversus. All rights reserved.</p>
          <div >
          <Image
                                        src='/assets/twitter.svg'
                                        alt='twitter logo'
                                        width={25}
                                        height={25}
                                />
                                    <Image
                                        src='/assets/linkedin.svg'
                                        alt='linkedin logo'
                                        width={25}
                                        height={25}
                                />
                                    <Image
                                        src='/assets/instagram.svg'
                                        alt='instagram logo'
                                        width={25}
                                        height={25}
                                />
                                    <Image
                                        src='/assets/facebook.svg'
                                        alt='facebook logo'
                                        width={25}
                                        height={25}
                                />

          </div>
          </section>

        </footer>)
};

export default FOOTER;
