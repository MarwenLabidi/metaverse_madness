"use client";
import Image from "next/image";

import React from "react";
import { header } from "./index.module.css";

const HEADER = () => {
        return (
                <div className={header}>
                        <button>
                                <Image
                                        src='/assets/search.svg'
                                        alt='search logo'
                                        width={25}
                                        height={25}
                                />
                        </button>
                        <h2>METAVERSUS</h2>
                        <button>
                        <Image
                                        src='/assets/menu.svg'
                                        alt='menu '
                                        width={25}
                                        height={25}
                                />
                        </button>
                </div>
        );
};

export default HEADER;
