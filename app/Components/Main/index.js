import React from "react";
import INTRODUCTION from "./Components/Introduction/index";
import WORLD from "./Components/Wold/index";
import GETSTARTED from "./Components/GetStarted/index";
import WHATSNEW from "./Components/WhatsNew/index";
import MAP from "./Components/Map/index";
import INSIGHT from "./Components/CL-Insight/index";
import ABOUT from "./Components/About/index";
import {main} from './index.module.css'

const MAIN = () => {
        return (
                <main className={main}>
                        <INTRODUCTION />
                        <WORLD />
                        <GETSTARTED />
                        <WHATSNEW />
                        <MAP />
                        <INSIGHT />
                        <ABOUT />
                </main>
        );
};

export default MAIN;
