import React from "react";
import {
        introduction,
        letterD,
        title,
        coverSection,
        cover,
        stamp,
        secondetitle,
        socialMedia,
        image
} from "./index.module.css";
import Image from "next/image";

const INTRODUCTION = () => {
        return (
                <div className={introduction}>
                        <section>
                                <div>
                                        <p>| Home</p>
                                        <div className={title}>
                                                <h1>METAVERSE</h1>
                                                <div className={secondetitle} >
                                                        <h1>MA</h1>
                                                        <div
                                                                className={
                                                                        letterD
                                                                }></div>
                                                        <h1>NESS</h1>
                                                </div>
                                        </div>
                                        <div className={socialMedia}>
                                                <Image
                                                        src='/assets/twitter.svg'
                                                        alt='twitter logo'
                                                        width={25}
                                                        height={25}
                                                />
                                                <Image
                                                        src='/assets/linkedin.svg'
                                                        alt='linkedin logo'
                                                        width={25}
                                                        height={25}
                                                />
                                                <Image
                                                        src='/assets/instagram.svg'
                                                        alt='instagram logo'
                                                        width={25}
                                                        height={25}
                                                />
                                                <Image
                                                        src='/assets/facebook.svg'
                                                        alt='facebook logo'
                                                        width={25}
                                                        height={25}
                                                />
                                        </div>
                                </div>
                                <div className={coverSection}>
                                        <Image
                                                className={cover}
                                                src='/assets/cover.png'
                                                alt='cover '
                                                fill
                                        />
                                        <Image
                                         className={stamp}
                                                src='/assets/stamp.png'
                                                alt='stamp'
                                                width={100}
                                                height={100}
                                        />
                                </div>
                        </section>
                        <section>
                                <p>| About Metaverus</p>
                                <h2>
                                        <span>Metaverse</span> is a new thing in
                                        the future, where you can enjoy the
                                        virtual world by feeling like it's
                                        really real, you can feel what you feel
                                        in this metaverse world, because this is
                                        really the{" "}
                                        <span>madness of the metaverse</span> of
                                        today, using only <span>VR</span>{" "}
                                        devices you can easily explore the
                                        metaverse world you want, turn your
                                        dreams into reality. Let's{" "}
                                        <span>explore</span> the madness of the
                                        metaverse by scrolling down
                                </h2>
                                <Image
                                className={image}
                                        src='/assets/arrow-down.svg'
                                        alt='arrow down '
                                        width={25}
                                        height={25}
                                />
                        </section>
                </div>
        );
};

export default INTRODUCTION;
