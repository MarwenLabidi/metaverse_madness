import { test } from "./lambdaTest-setup.js";

test("test", async ({ page }) => {
        await page.goto(
                "https://metaverse-madness-6130h7gra-marwenlabidi.vercel.app/"
        );
        await page.getByRole("button", { name: "search logo" }).click();
        await page.getByRole("button", { name: "menu" }).click();
        await page
                .locator("section")
                .filter({ hasText: "| HomeMETAVERSEMANESS" })
                .getByRole("img", { name: "twitter logo" })
                .click();
        await page
                .locator("section")
                .filter({ hasText: "| HomeMETAVERSEMANESS" })
                .getByRole("img", { name: "linkedin logo" })
                .click();
        await page
                .locator("section")
                .filter({ hasText: "| HomeMETAVERSEMANESS" })
                .getByRole("img", { name: "instagram logo" })
                .click();
});
